require "./wakdata/action"
require "./wakdata/blueprint"
require "./wakdata/collectible_resource"
require "./wakdata/harvest_loot"
require "./wakdata/item_property"
require "./wakdata/item_type"
require "./wakdata/item"
require "./wakdata/job_item"
require "./wakdata/recipe_category"
require "./wakdata/recipe_ingredient"
require "./wakdata/recipe_result"
require "./wakdata/recipe"
require "./wakdata/resource_type"
require "./wakdata/resource"
require "./wakdata/state"
require "git-version-gen/src/git-version-gen.cr"

module Wakdata
  GitVersionGen.generate_version __DIR__
end
