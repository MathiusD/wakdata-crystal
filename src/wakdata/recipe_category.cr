require "struct-gen"

require "./with"

StructGen.generate_struct Wakdata::Definition::RecipeCategory, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_archive,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_no_craft,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_hidden,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  xp_factor,
  attribute_class: Float32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_innate,
  attribute_class: Bool,
}

StructGen.generate_struct Wakdata::RecipeCategory, {
  is_module:            true,
  is_required:          true,
  base_module:          Wakdata,
  attribute_name:       definition,
  module_generics_args: Wakdata::Definition::RecipeCategory,
  attribute_class:      Wakdata::Definition::RecipeCategory,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  title,
  attribute_class: Wakdata::Traduction,
}
