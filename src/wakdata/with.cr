require "struct-gen"

require "./graphic_parameters"
require "./traduction"

StructGen.generate_with_required_field Wakdata, Definition(Def), definition, Def, "definition"
StructGen.generate_with_required_field Wakdata, Description, description, Wakdata::Traduction, "description"
StructGen.generate_with_required_field Wakdata, GraphicParameters, graphic_parameters, Wakdata::GraphicParameters, "graphicParameters"
StructGen.generate_with_required_field Wakdata, Id, id, UInt32, "id"
StructGen.generate_with_required_field Wakdata, ItemId, item_id, UInt32, "itemId"

module Wakdata::With::ItemId
  def item(items : Array(Wakdata::Item)) : Wakdata::Item?
    items.find? { |item|
      item.definition.item.id == self.item_id
    }
  end

  def item(jobItems : Array(Wakdata::JobItem)) : Wakdata::JobItem?
    jobItems.find? { |jobItem|
      jobItem.definition.id == self.item_id
    }
  end

  def item(items : Array(Wakdata::Item), jobItems : Array(Wakdata::JobItem)) : (Wakdata::Item | Wakdata::JobItem)?
    item = item(items)
    if item != nil
      item
    else
      items(jobItems)
    end
  end
end

StructGen.generate_with_required_field Wakdata, ItemTypeId, item_type_id, UInt32, "itemTypeId"

module Wakdata::With::ItemTypeId
  def item_type(item_types : Array(Wakdata::ItemType)) : Wakdata::ItemType?
    item_types.find? { |item_type|
      item_type.definition.id == self.item_type_id
    }
  end
end

StructGen.generate_with_required_field Wakdata, Title, title, Wakdata::Traduction, "title"
StructGen.generate_with_required_field Wakdata, Level, level, UInt16, "level"
StructGen.generate_with_required_field Wakdata, Quantity, quantity, UInt32, "quantity"
StructGen.generate_with_required_field Wakdata, Rarity, rarity, UInt8, "rarity"
StructGen.generate_with_required_field Wakdata, RecipeId, recipe_id, UInt32, "recipeId"

module Wakdata::With::RecipeId
  def recipe(item_types : Array(Wakdata::Recipe)) : Wakdata::Recipe?
    recipes.find? { |recipe|
      recipe.definition.id == self.recipe_id
    }
  end
end

StructGen.generate_with_required_field Wakdata, Title, title, Wakdata::Traduction, "title"
StructGen.generate_with_required_field Wakdata, ResourceTypeId, resource_type_id, UInt32, "resourceType"

module Wakdata::With::ResourceTypeId
  def resource_type(resource_types : Array(Wakdata::ResourceType)) : Wakdata::ResourceType?
    resource_types.find? { |resource_type|
      resource_type.definition.resource_type_id == self.recipe_id
    }
  end
end

StructGen.generate_with_required_field Wakdata, RecipeCategoryId, recipe_category_id, UInt32, "categoryId"

module Wakdata::With::RecipeCategoryId
  def recipe_category(recipe_categories : Array(Wakdata::RecipeCategory)) : Wakdata::RecipeCategory?
    recipe_categories.find? { |recipe_category|
      recipe_category.definition.resource_type_id == self.recipe_category_id
    }
  end
end
