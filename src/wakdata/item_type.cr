require "struct-gen"

require "./with"
require "./with/optional"

StructGen.generate_struct Wakdata::Definition::ItemType, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     false,
  attribute_name:  parent_id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  equipment_positions,
  attribute_class: Array(String),
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  equipment_disabled_positions,
  attribute_class: Array(String),
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_recyclable,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_visible_in_animation,
  attribute_class: Bool,
}

StructGen.generate_struct Wakdata::ItemType, {
  is_module:            true,
  is_required:          true,
  base_module:          Wakdata,
  attribute_name:       definition,
  module_generics_args: Wakdata::Definition::ItemType,
  attribute_class:      Wakdata::Definition::ItemType,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  title,
  attribute_class: Wakdata::Traduction,
}
