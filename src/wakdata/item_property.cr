require "struct-gen"

require "./with"

StructGen.generate_struct Wakdata::ItemProperty, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  name,
  attribute_class: String,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  description,
  attribute_class: String,
}
