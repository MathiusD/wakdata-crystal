require "struct-gen"

StructGen.generate_struct Wakdata::Traduction, {
  is_module:       false,
  is_required:     true,
  attribute_name:  fr,
  attribute_class: String,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  en,
  attribute_class: String,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  es,
  attribute_class: String,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  pt,
  attribute_class: String,
}
