require "struct-gen"

require "./with"
require "./with/optional"

StructGen.generate_struct Wakdata::Definition::State, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}

StructGen.generate_struct Wakdata::State, {
  is_module:            true,
  is_required:          true,
  base_module:          Wakdata,
  attribute_name:       definition,
  module_generics_args: Wakdata::Definition::State,
  attribute_class:      Wakdata::Definition::State,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  title,
  attribute_class: Wakdata::Traduction,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  description,
  attribute_class: Wakdata::Traduction,
}
