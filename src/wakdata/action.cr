require "struct-gen"

require "./with"
require "./with/optional"

StructGen.generate_struct Wakdata::Definition::Action, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  effect,
  attribute_class: String,
}

StructGen.generate_struct Wakdata::Action, {
  is_module:            true,
  is_required:          true,
  base_module:          Wakdata,
  module_generics_args: Wakdata::Definition::Action,
  attribute_name:       definition,
  attribute_class:      Wakdata::Definition::Action,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  description,
  attribute_class: Wakdata::Traduction,
}
