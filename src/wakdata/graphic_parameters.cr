require "struct-gen"

StructGen.generate_struct Wakdata::GraphicParameters, {
  is_module:       false,
  is_required:     true,
  attribute_name:  gfx_id,
  attribute_class: Int32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  female_gfx_id,
  attribute_class: Int32,
}
