require "struct-gen"

require "./with"

StructGen.generate_struct Wakdata::RecipeIngredient, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  item_id,
  attribute_class: UInt32,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  recipe_id,
  attribute_class: UInt32,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  quantity,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  ingredient_order,
  attribute_class: UInt8,
}
