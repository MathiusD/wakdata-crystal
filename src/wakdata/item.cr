require "struct-gen"

require "./effect"

require "./with"
require "./with/optional"

StructGen.generate_struct Wakdata::SubSubDefinition::Item::Item::BaseParameters, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  rarity,
  attribute_class: UInt8,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  item_type_id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  item_set_id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  bind_type,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  minimum_shard_slot_number,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  maximum_shard_slot_number,
  attribute_class: UInt8,
}

StructGen.generate_struct Wakdata::SubSubDefinition::Item::Item::UseParameters, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_cost_ap,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_cost_mp,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_cost_wp,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_range_min,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_range_max,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_test_free_cell,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_test_los,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_test_only_line,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_test_no_border_cell,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_world_target,
  attribute_class: UInt32,
}

StructGen.generate_struct Wakdata::SubSubDefinition::Item::Item::SublimationParameters, {
  is_module:       false,
  is_required:     true,
  attribute_name:  slot_color_pattern,
  attribute_class: Array(UInt8),
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_epic,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_relic,
  attribute_class: Bool,
}

StructGen.generate_struct Wakdata::SubSubDefinition::Item::Item::ShardsParameters, {
  is_module:       false,
  is_required:     true,
  attribute_name:  color,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  double_bonus_position,
  attribute_class: Array(Int8),
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  shard_leveling_curve,
  attribute_class: Array(UInt16),
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  shard_level_requirement,
  attribute_class: Array(UInt16),
}

StructGen.generate_struct Wakdata::SubDefinition::Item::Item, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  level,
  attribute_class: UInt16,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  graphic_parameters,
  attribute_class: Wakdata::GraphicParameters,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  base_parameters,
  attribute_class: Wakdata::SubSubDefinition::Item::Item::BaseParameters,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_parameters,
  attribute_class: Wakdata::SubSubDefinition::Item::Item::UseParameters,
}, {
  is_module:       false,
  is_required:     false,
  attribute_name:  shards_parameters,
  attribute_class: Wakdata::SubSubDefinition::Item::Item::ShardsParameters,
}, {
  is_module:       false,
  is_required:     false,
  attribute_name:  sublimation_parameters,
  attribute_class: Wakdata::SubSubDefinition::Item::Item::SublimationParameters,
}, {
  is_module:       false,
  is_required:     false,
  attribute_name:  properties,
  attribute_class: Array(UInt8),
}

StructGen.generate_struct Wakdata::SubDefinition::Item::Effect, {
  is_module:       false,
  is_required:     true,
  attribute_name:  effect,
  attribute_class: Wakdata::Effect,
}, {
  is_module:       false,
  is_required:     false,
  attribute_name:  sub_effects,
  attribute_class: Array(Wakdata::SubDefinition::Item::Effect),
}

StructGen.generate_struct Wakdata::Definition::Item, {
  is_module:       false,
  is_required:     true,
  attribute_name:  item,
  attribute_class: Wakdata::SubDefinition::Item::Item,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_effects,
  attribute_class: Array(Wakdata::SubDefinition::Item::Effect),
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  use_critical_effects,
  attribute_class: Array(Wakdata::SubDefinition::Item::Effect),
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  equip_effects,
  attribute_class: Array(Wakdata::SubDefinition::Item::Effect),
}

StructGen.generate_struct Wakdata::Item, {
  is_module:            true,
  is_required:          true,
  base_module:          Wakdata,
  attribute_name:       definition,
  module_generics_args: Wakdata::Definition::Item,
  attribute_class:      Wakdata::Definition::Item,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  title,
  attribute_class: Wakdata::Traduction,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  description,
  attribute_class: Wakdata::Traduction,
}
