require "struct-gen"

StructGen.generate_struct Wakdata::Blueprint, {
  is_module:       false,
  is_required:     true,
  attribute_name:  blueprint_id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  serialized_name: "recipeId",
  attribute_name:  recipes_ids,
  attribute_class: Array(UInt32),
}
