require "struct-gen"

require "./with"

StructGen.generate_struct Wakdata::Recipe, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  level,
  attribute_class: UInt16,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  recipe_category_id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  xp_ratio,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_upgrade,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  upgrade_item_id,
  attribute_class: UInt32,
}
