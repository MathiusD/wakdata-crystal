require "struct-gen"

require "./with"
require "./with/optional"

StructGen.generate_struct Wakdata::Definition::Effect, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  action_id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  area_shape,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  area_size,
  attribute_class: Array(Int8),
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  params,
  attribute_class: Array(Float32),
}

StructGen.generate_struct Wakdata::Effect, {
  is_module:            true,
  is_required:          false,
  base_module:          Wakdata,
  attribute_name:       definition,
  module_generics_args: Wakdata::Definition::Effect,
  attribute_class:      Wakdata::Definition::Effect,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  description,
  attribute_class: Wakdata::Traduction,
}
