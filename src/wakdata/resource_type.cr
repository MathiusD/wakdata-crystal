require "struct-gen"

require "./with"

StructGen.generate_struct Wakdata::Definition::ResourceType, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  affect_wakfu,
  attribute_class: Bool,
}

StructGen.generate_struct Wakdata::ResourceType, {
  is_module:            true,
  is_required:          true,
  base_module:          Wakdata,
  attribute_name:       definition,
  module_generics_args: Wakdata::Definition::ResourceType,
  attribute_class:      Wakdata::Definition::ResourceType,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  title,
  attribute_class: Wakdata::Traduction,
}
