require "struct-gen"

require "./with"
require "./with/optional"

StructGen.generate_struct Wakdata::Definition::JobItem, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  level,
  attribute_class: UInt16,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  graphic_parameters,
  attribute_class: Wakdata::GraphicParameters,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  rarity,
  attribute_class: UInt8,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  item_type_id,
  attribute_class: UInt8,
}

StructGen.generate_struct Wakdata::JobItem, {
  is_module:            true,
  is_required:          true,
  base_module:          Wakdata,
  attribute_name:       definition,
  module_generics_args: Wakdata::Definition::JobItem,
  attribute_class:      Wakdata::Definition::JobItem,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  title,
  attribute_class: Wakdata::Traduction,
}, {
  is_module:       true,
  is_required:     false,
  base_module:     Wakdata,
  attribute_name:  description,
  attribute_class: Wakdata::Traduction,
}
