require "struct-gen"

require "./with"

StructGen.generate_struct Wakdata::RecipeResult, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  recipe_id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  producted_item_id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  product_order,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  producted_item_quantity,
  attribute_class: UInt32,
}
