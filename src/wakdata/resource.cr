require "struct-gen"

require "./with"

StructGen.generate_struct Wakdata::Definition::Resource, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  id,
  attribute_class: UInt32,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  resource_type_id,
  attribute_class: UInt32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  is_blocking,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  ideal_rain_range_min,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  ideal_rain_range_max,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  ideal_temperature_range_min,
  attribute_class: Int8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  ideal_temperature_range_max,
  attribute_class: Int8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  icon_gfx_id,
  attribute_class: Int32,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  last_evolution_step,
  attribute_class: UInt8,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  usable_by_heroes,
  attribute_class: Bool,
}, {
  is_module:       false,
  is_required:     true,
  attribute_name:  ideal_rain,
  attribute_class: UInt8,
}

StructGen.generate_struct Wakdata::Resource, {
  is_module:            true,
  is_required:          true,
  base_module:          Wakdata,
  attribute_name:       definition,
  module_generics_args: Wakdata::Definition::Resource,
  attribute_class:      Wakdata::Definition::Resource,
}, {
  is_module:       true,
  is_required:     true,
  base_module:     Wakdata,
  attribute_name:  title,
  attribute_class: Wakdata::Traduction,
}
