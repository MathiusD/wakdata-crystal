require "struct-gen"

require "../graphic_parameters"
require "../traduction"

StructGen.generate_with_optional_field Wakdata,
  Definition(Def),
  definition,
  Def,
  "definition"
StructGen.generate_with_optional_field Wakdata,
  Description,
  description,
  Wakdata::Traduction,
  "description"
StructGen.generate_with_optional_field Wakdata,
  GraphicParameters,
  graphic_parameters,
  Wakdata::GraphicParameters,
  "graphicParameters"
StructGen.generate_with_optional_field Wakdata,
  Title,
  title,
  Wakdata::Traduction,
  "title"
