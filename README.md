# Wakdata - Crystal

[![Pipeline Status](https://gitlab.com/MathiusD/wakdata-crystal/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/wakdata-crystal/-/pipelines)
[![Documentation](https://img.shields.io/badge/docs-available-brightgreen.svg)](https://mathiusd.gitlab.io/wakdata-crystal)

Wakdata with definitions for public data of MMORPG Wakfu from JSON files distributed in Wakfu CDN
